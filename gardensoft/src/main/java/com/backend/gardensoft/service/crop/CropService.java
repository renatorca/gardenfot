package com.backend.gardensoft.service.crop;


import com.backend.gardensoft.entity.Crop;
import com.backend.gardensoft.entity.Plot;

import java.util.ArrayList;

public interface CropService {
    public ArrayList<Crop> findByGarden(Integer idGarden);
    public ArrayList<Crop> findByGardenAndStatus(Integer idGarden);
    public Crop createCrop(Crop crop);
    public Crop updateCrop(Crop crop);
    public Crop deleteCrop(Integer idCrop);
    public Crop harvestCrop(Integer idCrop);
    public Crop changePlot(Crop crop, Plot plot);
}
