package com.backend.gardensoft.service.cropcontrol;

import com.backend.gardensoft.entity.Crop;
import com.backend.gardensoft.entity.CropControl;
import com.backend.gardensoft.repository.CropControlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CropControlServiceImpl implements CropControlService{
    @Autowired
    private CropControlRepository cropControlRepository;
    @Override
    public ArrayList<CropControl> getAllByGarden(Integer idGarden, Boolean isActive) {
        return cropControlRepository.findAllByCropVegetableGardenIdGardenAndIsActive(idGarden, isActive);
    }

    @Override
    public ArrayList<CropControl> getAllByVegetable(Integer idVegetable, Boolean isActive) {
        return cropControlRepository.findAllByCropVegetableIdVegetableAndIsActive(idVegetable, isActive);
    }

    @Override
    public CropControl createCropControl(CropControl cropControl) {
        return cropControlRepository.save(cropControl);
    }

    @Override
    public CropControl updateCropControl(CropControl cropControl) {
        CropControl original = cropControlRepository.findByIdCropControlAndIsActive(cropControl.getIdCropControl(), true);
        if(original!=null){
            return cropControlRepository.save(cropControl);
        }
        return null;
    }

    @Override
    public CropControl deleteCropControl(Integer idCropControl) {
        CropControl original = cropControlRepository.findByIdCropControlAndIsActive(idCropControl, true);
        if(original!=null){
            original.setIsActive(false);
            return cropControlRepository.save(original);
        }
        return null;
    }

    @Override
    public void deleteCropControlsByCrop(Integer idCrop) {
        ArrayList<CropControl> list = cropControlRepository.findAllByCropIdCropAndIsActive(idCrop, true);
        if(list.size()>0){
            int i=0;
            for(i=0; i<list.size(); i++){
                CropControl cropControl = list.get(i);
                cropControl.setIsActive(false);
                cropControlRepository.save(cropControl);
            }
        }
    }
}
