package com.backend.gardensoft.service.rule;

import com.backend.gardensoft.entity.Rule;

import java.util.ArrayList;

public interface RuleService {
    public ArrayList<Rule> findByGarden(Integer idGarden);
    public Rule createRule(Rule rule);
    public Rule updateRule(Rule rule);
    public Rule deleteRule(Integer idRule);
}
