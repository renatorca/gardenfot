package com.backend.gardensoft.service.advice;

import com.backend.gardensoft.dto.DecisionDTO;
import com.backend.gardensoft.entity.Advice;
import com.backend.gardensoft.entity.CropControl;
import com.backend.gardensoft.entity.Decision;
import com.backend.gardensoft.repository.AdviceRepository;
import com.backend.gardensoft.repository.DecisionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class AdviceServiceImpl implements AdviceService{
    @Autowired
    private AdviceRepository adviceRepository;
    @Autowired
    private DecisionRepository decisionRepository;
    @Override
    public Advice acceptAdvice(Integer idAdvice) {
        Advice original = adviceRepository.findByIdAdvice(idAdvice);
        original.setIsAccepted(true);
        LocalDateTime time = LocalDateTime.now();
        original.setAnswerTime(time);
        original.setIsAnswered(true);
        return adviceRepository.save(original);
    }

    @Override
    public Advice denyAdvice(Integer idAdvice, DecisionDTO cropControl) {
        Advice original = adviceRepository.findByIdAdvice(idAdvice);
        original.setIsAccepted(false);
        LocalDateTime time = LocalDateTime.now();
        original.setAnswerTime(time);
        original.setIsAnswered(true);
        Advice saved = adviceRepository.save(original);
        Decision decision = new Decision();
        decision.setIdDecision(null);
        decision.setAdvice(saved);
        decision.setCropControl(cropControl.getCropControl());
        decision.setOnAction(cropControl.getOnAction());
        decisionRepository.save(decision);
        return saved;
    }
}
