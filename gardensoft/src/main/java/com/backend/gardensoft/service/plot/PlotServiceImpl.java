package com.backend.gardensoft.service.plot;

import com.backend.gardensoft.entity.Plot;
import com.backend.gardensoft.repository.PlotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
@Service
public class PlotServiceImpl implements PlotService{
    @Autowired
    private PlotRepository plotRepository;

    @Override
    @Transactional(readOnly = true)
    public ArrayList<Plot> findByGardenAndStage(Integer idGarden, Integer idCropStage) {
        return plotRepository.findAllByIsUsedAndGardenIdGardenAndCropStageIdCropStage(false, idGarden, idCropStage);
    }

    @Override
    @Transactional(readOnly = true)
    public ArrayList<Plot> findByGarden(Integer idPlot, Integer idGarden) {
        return plotRepository.findAllByIsUsedAndIdPlotNotAndGardenIdGarden(false, idPlot, idGarden);
    }

    @Override
    @Transactional
    public Plot createPlot(Plot plot) {
        return plotRepository.save(plot);
    }

    @Override
    @Transactional
    public Plot usePlot(Plot plot) {
        if(!plot.getIsUsed()){
            plot.setIsUsed(true);
            return plotRepository.save(plot);
        }
        return null;
    }

    @Override
    @Transactional
    public Plot freePlot(Plot plot) {
        if(plot.getIsUsed()){
            plot.setIsUsed(false);
            return plotRepository.save(plot);
        }
        return null;
    }
}
