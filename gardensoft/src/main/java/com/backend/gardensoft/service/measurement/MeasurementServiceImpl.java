package com.backend.gardensoft.service.measurement;

import com.backend.gardensoft.dto.AdviceDTO;
import com.backend.gardensoft.entity.*;
import com.backend.gardensoft.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;

@Service
public class MeasurementServiceImpl implements MeasurementService{
    @Autowired
    private MeasurementRepository measurementRepository;
    @Autowired
    private ControlActionRepository controlActionRepository;
    @Autowired
    private PredecessorListRepository predecessorListRepository;
    @Autowired
    private AdviceRepository adviceRepository;
    @Autowired
    private FactListRepository factListRepository;
    @Override
    public AdviceDTO advice(Integer idGarden) {
        int i, j, k, l, m;
        boolean inRules, actionFounded=true;
        ArrayList<Measurement> facts = new ArrayList<>();
        ArrayList<Measurement> measurements = measurementRepository.getAllByCropVegetableGardenIdGardenAndIsCheckedOrderByCropIdCropAsc(idGarden, false);
        ArrayList<ControlAction> controlActions = controlActionRepository.findAllByRuleVegetableGardenIdGardenAndIsLastAndIsActiveOrderByCropControlCropIdCropAsc(idGarden, true, true);
        for(i=0; i<measurements.size(); i++){
            inRules=false;
            for(j=0; j< controlActions.size(); j++){
                //se evalúa si se trata del mismo cultivo
                if(measurements.get(i).getCrop().getIdCrop()==controlActions.get(j).getCropControl().getCrop().getIdCrop()) {
                    //se evalua si la var climatica de la regla del control corresponde a la var climatica de la medicion
                    if (controlActions.get(j).getRule().getClimaticVariable().getIdClimaticVariable() == measurements.get(i).getClimaticVariable().getIdClimaticVariable()) {
                        if (controlActions.get(j).getAttribute().equals("Menor")) {
                            //si es menor
                            if (measurements.get(i).getValue() < controlActions.get(j).getRule().getMinValue()) {
                                inRules = true;
                                actionFounded=true;
                            }
                        } else if (controlActions.get(j).getAttribute().equals("Mayor")) {
                            //si es mayor
                            if (measurements.get(i).getValue() > controlActions.get(j).getRule().getMaxValue()) {
                                inRules = true;
                                actionFounded=true;
                            }
                        } else {
                            //si es igual
                            if (measurements.get(i).getValue() >= controlActions.get(j).getRule().getMinValue() && measurements.get(i).getValue() <= controlActions.get(j).getRule().getMaxValue()) {
                                inRules = true;
                                actionFounded=true;
                            }
                        }
                        if (inRules) {
                            //si se cumple con la regla, se obtiene la lista de las demas mediciones del cultivo
                            ArrayList<Measurement> measurementsFiltered = measurementRepository.getAllByIdMeasurementNotAndCropIdCropAndIsCheckedOrderByCropIdCropAsc(measurements.get(i).getIdMeasurement(), measurements.get(i).getCrop().getIdCrop(), false);
                            ArrayList<PredecessorList> predecessorsList = predecessorListRepository.findAllByControlActionIdControlActionAndIsActive(controlActions.get(j).getIdControlAction(), true);
                            if (predecessorsList.size() > 0) {
                                k = 0;
                                l = 0;
                                for (l = 0; l < predecessorsList.size(); l++) {
                                    boolean ruleSatisfied = false;
                                    for (k = 0; k < measurementsFiltered.size(); k++) {
                                        //comparar variables climaticas
                                        if (predecessorsList.get(l).getPredecessorControlAction().getRule().getClimaticVariable().getIdClimaticVariable() == measurementsFiltered.get(k).getClimaticVariable().getIdClimaticVariable()) {
                                            //comparar attribute
                                            if (predecessorsList.get(l).getPredecessorControlAction().getAttribute().equals("Mayor")) {
                                                if (measurementsFiltered.get(k).getValue() > predecessorsList.get(l).getPredecessorControlAction().getRule().getMaxValue()) {
                                                    //se realiza el and logico y se agrega a la lista de reglas cumplidas
                                                    ruleSatisfied = true;
                                                    break;
                                                }
                                            } else if (predecessorsList.get(l).getPredecessorControlAction().getAttribute().equals("Menor")) {
                                                if (measurementsFiltered.get(k).getValue() < predecessorsList.get(l).getPredecessorControlAction().getRule().getMinValue()) {
                                                    //se realiza el and logico y se agrega a la lista de reglas cumplidas
                                                    ruleSatisfied = true;
                                                    break;
                                                }
                                            } else {
                                                if (measurementsFiltered.get(k).getValue() >= predecessorsList.get(l).getPredecessorControlAction().getRule().getMinValue() && measurementsFiltered.get(k).getValue() <= predecessorsList.get(l).getPredecessorControlAction().getRule().getMaxValue()) {
                                                    //se realiza el and logico y se agrega a la lista de reglas cumplidas
                                                    ruleSatisfied = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    actionFounded = actionFounded && ruleSatisfied;
                                    if (actionFounded) {
                                        facts.add(measurementsFiltered.get(k));
                                    } else {
                                        //no se ha cumplido con lo requerido para activar la acción
                                        facts.clear();
                                        inRules=false;
                                        break;
                                    }
                                }
                                if (actionFounded) {
                                    //si ha llegado con true es decir que existe una regla para todas las mediciones
                                    Advice advice = new Advice();
                                    advice.setIdAdvice(null);
                                    advice.setAnswerTime(null);
                                    advice.setIsAccepted(null);
                                    advice.setIsAnswered(false);
                                    LocalDateTime time = LocalDateTime.now();
                                    advice.setTime(time);
                                    advice.setControlAction(controlActions.get(j));
                                    advice.setUser(null);
                                    Advice saved = adviceRepository.save(advice);
                                    //se crea la respuesta para front
                                    AdviceDTO response = new AdviceDTO();
                                    response.setCropControl(controlActions.get(j).getCropControl());
                                    facts.add(measurements.get(i));
                                    response.setMeasurements(facts);
                                    response.setOnAction(controlActions.get(j).getOnAction());
                                    response.setIdAdvice(saved.getIdAdvice());
                                    //se tiene que marcar como revisadas las mediciones
                                    //se tiene que registrar en fact_list las mediciones de la recomendación
                                    for(m=0; m< facts.size(); m++){
                                        facts.get(m).setIsChecked(true);
                                        facts.get(m).setOutOfThreshold(true);
                                        Measurement measurementSaved = measurementRepository.save(facts.get(m));
                                        FactList factList = new FactList();
                                        factList.setIdFactList(null);
                                        factList.setAdvice(saved);
                                        factList.setMeasurement(measurementSaved);
                                        FactList factListSaved = factListRepository.save(factList);
                                    }

                                    return response;
                                }
                            }
                            else{
                                //si no tiene predecesoras se tiene que repetir el proceso
                                Advice advice = new Advice();
                                advice.setIdAdvice(null);
                                advice.setAnswerTime(null);
                                advice.setIsAccepted(null);
                                advice.setIsAnswered(false);
                                LocalDateTime time = LocalDateTime.now();
                                advice.setTime(time);
                                advice.setControlAction(controlActions.get(j));
                                advice.setUser(null);
                                Advice saved = adviceRepository.save(advice);
                                //se crea la respuesta para front
                                AdviceDTO response = new AdviceDTO();
                                response.setCropControl(controlActions.get(j).getCropControl());
                                facts.add(measurements.get(i));
                                response.setMeasurements(facts);
                                response.setOnAction(controlActions.get(j).getOnAction());
                                response.setIdAdvice(saved.getIdAdvice());
                                //se tiene que marcar como revisadas las mediciones
                                //se tiene que registrar en fact_list las mediciones de la recomendación
                                for(m=0; m< facts.size(); m++){
                                    facts.get(m).setIsChecked(true);
                                    facts.get(m).setOutOfThreshold(true);
                                    Measurement measurementSaved = measurementRepository.save(facts.get(m));
                                    FactList factList = new FactList();
                                    factList.setIdFactList(null);
                                    factList.setAdvice(saved);
                                    factList.setMeasurement(measurementSaved);
                                    FactList factListSaved = factListRepository.save(factList);
                                }

                                return response;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
}
