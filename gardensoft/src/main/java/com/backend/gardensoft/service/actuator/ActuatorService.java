package com.backend.gardensoft.service.actuator;

import com.backend.gardensoft.entity.Actuator;
import com.backend.gardensoft.entity.CropControl;

public interface ActuatorService {
    public Actuator getAll();
    public Actuator update(CropControl cropControl);
}
