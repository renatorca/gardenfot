package com.backend.gardensoft.service.predecessorlist;

import com.backend.gardensoft.entity.PredecessorList;
import com.backend.gardensoft.repository.PredecessorListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
public class PredecessorListServiceImpl implements PredecessorListService{
    @Autowired
    private PredecessorListRepository predecessorListRepository;

    @Override
    @Transactional
    public PredecessorList createPredecessorList(PredecessorList predecessorList) {
        return predecessorListRepository.save(predecessorList);
    }

    @Override
    @Transactional
    public PredecessorList deletePredecessorList(Integer idPredecessorList) {
        PredecessorList original = predecessorListRepository.findByIdPredecessorListAndIsActive(idPredecessorList, true);
        if(original!=null){
            original.setIsActive(false);
            return predecessorListRepository.save(original);
        }
        return null;
    }

    @Override
    public PredecessorList deleteByIds(Integer idControlAction, Integer idPredecessorControlAction) {
        PredecessorList original = predecessorListRepository.findByControlActionIdControlActionAndPredecessorControlActionIdControlActionAndIsActive(idControlAction, idPredecessorControlAction, true);
        if(original!=null){
            original.setIsActive(false);
            return predecessorListRepository.save(original);
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public ArrayList<PredecessorList> getByControlAction(Integer idControlAction) {
        return predecessorListRepository.findAllByControlActionIdControlActionAndIsActive(idControlAction, true);
    }

    @Override
    public ArrayList<PredecessorList> getByPredecessorControlAction(Integer idPredecessorControlAction) {
        return predecessorListRepository.findAllByPredecessorControlActionIdControlActionAndIsActive(idPredecessorControlAction, true);
    }

    @Override
    public ArrayList<PredecessorList> deleteAllByPredecessorControlAction(Integer idPredecessorControlAction) {
        int i;
        ArrayList<PredecessorList> deletingList = getByPredecessorControlAction(idPredecessorControlAction);
        ArrayList<PredecessorList> aux = new ArrayList<>();
        if(deletingList!=null){
            for(i=0; i<deletingList.size(); i++){
                aux.add(deletePredecessorList(deletingList.get(i).getIdPredecessorList()));
            }
            return aux;
        }
        return null;
    }
}
