package com.backend.gardensoft.service.file;

import com.backend.gardensoft.entity.Measurement;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;

public interface BulkLoadService {
    public ArrayList<Measurement> save(MultipartFile file);
}
