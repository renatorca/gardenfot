package com.backend.gardensoft.service.crop;

import com.backend.gardensoft.entity.Crop;
import com.backend.gardensoft.entity.Plot;
import com.backend.gardensoft.entity.VegetableStatus;
import com.backend.gardensoft.repository.CropRepository;
import com.backend.gardensoft.repository.VegetableStatusRepository;
import com.backend.gardensoft.service.cropcontrol.CropControlService;
import com.backend.gardensoft.service.plot.PlotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
@Service
public class CropServiceImpl implements CropService{
    @Autowired
    private CropRepository cropRepository;
    @Autowired
    private VegetableStatusRepository vegetableStatusRepository;
    @Autowired
    private PlotService plotService;
    @Autowired
    private CropControlService cropControlService;
    @Override
    @Transactional(readOnly = true)
    public ArrayList<Crop> findByGarden(Integer idGarden) {
        return cropRepository.findAllByVegetableGardenIdGardenAndIsActive(idGarden, true);
    }

    @Override
    @Transactional(readOnly = true)
    public ArrayList<Crop> findByGardenAndStatus(Integer idGarden) {
        return cropRepository.findAllByVegetableGardenIdGardenAndVegetableStatusIdVegetableStatusAndIsActive(idGarden, 1, true);
    }

    @Override
    @Transactional
    public Crop createCrop(Crop crop) {
        plotService.usePlot(crop.getPlot());
        return cropRepository.save(crop);
    }

    @Override
    @Transactional
    public Crop updateCrop(Crop crop) {
        Crop original = cropRepository.findByIdCropAndIsActive(crop.getIdCrop(), true);
        if(original!=null){
            return cropRepository.save(crop);
        }
        return null;
    }

    @Override
    @Transactional
    public Crop deleteCrop(Integer idCrop) {
        Crop original = cropRepository.findByIdCropAndIsActive(idCrop, true);
        if(original!=null){
            original.setIsActive(false);
            plotService.freePlot(original.getPlot());
            cropControlService.deleteCropControlsByCrop(original.getIdCrop());
            return cropRepository.save(original);
        }
        return null;
    }

    @Override
    @Transactional
    public Crop harvestCrop(Integer idCrop) {
        VegetableStatus status = vegetableStatusRepository.findByName("Cosechado");
        Crop crop = cropRepository.findByIdCropAndIsActive(idCrop, true);
        if(crop!=null){
            crop.setVegetableStatus(status);
            plotService.freePlot(crop.getPlot());
            return cropRepository.save(crop);
        }
        return null;
    }

    @Override
    @Transactional
    public Crop changePlot(Crop crop, Plot plot) {
        Crop original = cropRepository.findByIdCropAndIsActive(crop.getIdCrop(), true);
        if(original!=null){
            plotService.freePlot(original.getPlot());
            original.setPlot(plot);
            plotService.usePlot(plot);
            return cropRepository.save(original);
        }
        return null;
    }
}
