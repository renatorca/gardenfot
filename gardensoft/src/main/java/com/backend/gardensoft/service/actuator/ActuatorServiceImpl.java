package com.backend.gardensoft.service.actuator;

import com.backend.gardensoft.entity.Actuator;
import com.backend.gardensoft.entity.CropControl;
import com.backend.gardensoft.repository.ActuatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class ActuatorServiceImpl implements ActuatorService{
    @Autowired
    private ActuatorRepository actuatorRepository;
    @Override
    public Actuator getAll() {
        return actuatorRepository.findByIdActuator(1);
    }

    @Override
    public Actuator update(CropControl cropControl) {
        Actuator original = actuatorRepository.findByIdActuator(1);
        original.setName(cropControl.getName());
        original.setAction(cropControl.getOnCommand());
        LocalDateTime time = LocalDateTime.now();
        time.minusHours(5);
        original.setTime(time);
        return actuatorRepository.save(original);
    }
}
