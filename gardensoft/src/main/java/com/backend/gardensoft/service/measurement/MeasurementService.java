package com.backend.gardensoft.service.measurement;

import com.backend.gardensoft.dto.AdviceDTO;
import com.backend.gardensoft.entity.CropControl;

public interface MeasurementService {
    public AdviceDTO advice(Integer idGarden);
}
