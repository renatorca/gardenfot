package com.backend.gardensoft.service.predecessorlist;

import com.backend.gardensoft.entity.PredecessorList;

import java.util.ArrayList;

public interface PredecessorListService {
    public PredecessorList createPredecessorList(PredecessorList predecessorList);
    public PredecessorList deletePredecessorList(Integer idPredecessorList);
    public PredecessorList deleteByIds(Integer idControlAction, Integer idPredecessorControlAction);
    public ArrayList<PredecessorList> getByControlAction(Integer idControlAction);
    public ArrayList<PredecessorList> getByPredecessorControlAction(Integer idPredecessorControlAction);
    public ArrayList<PredecessorList> deleteAllByPredecessorControlAction(Integer idPredecessorControlAction);
}
