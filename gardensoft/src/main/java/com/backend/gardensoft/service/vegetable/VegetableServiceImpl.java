package com.backend.gardensoft.service.vegetable;

import com.backend.gardensoft.dto.MeasurementDTO;
import com.backend.gardensoft.entity.Crop;
import com.backend.gardensoft.entity.Measurement;
import com.backend.gardensoft.entity.Vegetable;
import com.backend.gardensoft.repository.CropRepository;
import com.backend.gardensoft.repository.MeasurementRepository;
import com.backend.gardensoft.repository.VegetableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
public class VegetableServiceImpl implements VegetableService{
    @Autowired
    private VegetableRepository vegetableRepository;
    @Autowired
    private MeasurementRepository measurementRepository;
    @Autowired
    private CropRepository cropRepository;
    @Override
    @Transactional(readOnly = true)
    public ArrayList<Vegetable> findByGarden(Integer idGarden) {
        return vegetableRepository.findAllByGardenIdGardenAndIsActive(idGarden, true);
    }

    @Override
    @Transactional
    public Vegetable createVegetable(Vegetable vegetable) {
        return vegetableRepository.save(vegetable);
    }

    @Override
    @Transactional
    public Vegetable updateVegetable(Vegetable vegetable) {
        Vegetable original = vegetableRepository.findByIdVegetableAndIsActive(vegetable.getIdVegetable(), true);
        if(original!=null){
            return vegetableRepository.save(vegetable);
        }
        return null;
    }

    @Override
    @Transactional
    public Vegetable deleteVegetable(Integer idVegetable) {
        Vegetable original = vegetableRepository.findByIdVegetableAndIsActive(idVegetable, true);
        if(original!=null){
            original.setIsActive(false);
            return vegetableRepository.save(original);
        }
        return null;
    }

    @Override
    @Transactional
    public Vegetable addFavorites(Integer idVegetable) {
        Vegetable original = vegetableRepository.findByIdVegetableAndIsActive(idVegetable, true);
        if(original!=null){
            if(!original.getIsFavorite()) {
                original.setIsFavorite(true);
                return vegetableRepository.save(original);
            }
        }
        return null;
    }

    @Override
    @Transactional
    public Vegetable removeFavorites(Integer idVegetable) {
        Vegetable original = vegetableRepository.findByIdVegetableAndIsActive(idVegetable, true);
        if(original!=null){
            if(original.getIsFavorite()) {
                original.setIsFavorite(false);
                return vegetableRepository.save(original);
            }
        }
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public ArrayList<MeasurementDTO> getMeasurementByFavoriteVegetable(Integer idGarden) {
        int i;
        ArrayList<MeasurementDTO> list = new ArrayList<MeasurementDTO>();
        ArrayList<Crop> crops = cropRepository.findAllByVegetableGardenIdGardenAndIsActive(idGarden, true);
        if(crops.size()>0){
            for(i=0; i<crops.size();i++){
                if(crops.get(i).getVegetable().getIsFavorite()){
                    ArrayList<Measurement> measurements = measurementRepository.getAllByCropIdCrop(crops.get(i).getIdCrop());
                    MeasurementDTO value = new MeasurementDTO();
                    value.setVegetable(crops.get(i).getVegetable());
                    value.setMeasurements(measurements);
                    list.add(value);
                }
            }
            return list;
        }
        return null;
    }
}
