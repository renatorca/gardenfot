package com.backend.gardensoft.service.plot;

import com.backend.gardensoft.entity.Plot;

import java.util.ArrayList;

public interface PlotService {
    public ArrayList<Plot> findByGardenAndStage(Integer idGarden, Integer idCropStage);
    public ArrayList<Plot> findByGarden(Integer idPlot, Integer idGarden);
    public Plot createPlot(Plot plot);
    public Plot usePlot(Plot plot);
    public Plot freePlot(Plot plot);
}
