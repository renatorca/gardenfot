package com.backend.gardensoft.service.cropcontrol;

import com.backend.gardensoft.entity.CropControl;

import java.util.ArrayList;

public interface CropControlService {
    ArrayList<CropControl> getAllByGarden(Integer idGarden, Boolean isActive);
    ArrayList<CropControl> getAllByVegetable(Integer idVegetable, Boolean isActive);
    CropControl createCropControl(CropControl cropControl);
    CropControl updateCropControl(CropControl cropControl);
    CropControl deleteCropControl(Integer idCropControl);
    void deleteCropControlsByCrop(Integer idCrop);
}
