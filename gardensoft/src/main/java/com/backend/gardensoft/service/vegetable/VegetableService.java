package com.backend.gardensoft.service.vegetable;

import com.backend.gardensoft.dto.MeasurementDTO;
import com.backend.gardensoft.entity.Vegetable;

import java.util.ArrayList;

public interface VegetableService {
    public ArrayList<Vegetable> findByGarden(Integer idGarden);
    public Vegetable createVegetable(Vegetable vegetable);
    public Vegetable updateVegetable(Vegetable vegetable);
    public Vegetable deleteVegetable(Integer idVegetable);
    public Vegetable addFavorites(Integer idVegetable);
    public Vegetable removeFavorites(Integer idVegetable);
    public ArrayList<MeasurementDTO> getMeasurementByFavoriteVegetable(Integer idGarden);
}
