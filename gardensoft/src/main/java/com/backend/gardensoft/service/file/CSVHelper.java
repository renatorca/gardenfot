package com.backend.gardensoft.service.file;

import com.backend.gardensoft.dto.MeasurementFileDTO;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class CSVHelper {
    public static String TYPE = "csv";
    static String[] HEADERs = { "idMeasurement", "measurementTime", "value", "idClimaticVariable", "idCrop" };

    public static boolean hasCSVFormat(MultipartFile file) {

        if (!TYPE.equals(file.getContentType())) {
            return false;
        }

        return true;
    }

    public static List<MeasurementFileDTO> csvToMeasurements(InputStream is) {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
             CSVParser csvParser = new CSVParser(fileReader,
                     CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

            List<MeasurementFileDTO> measurements = new ArrayList<MeasurementFileDTO>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            for (CSVRecord csvRecord : csvRecords) {
                MeasurementFileDTO measurement = new MeasurementFileDTO(
                        Integer.parseInt(csvRecord.get("idMeasurement")),
                        LocalDateTime.parse(csvRecord.get("measurementTime"), formatter),
                        Float.parseFloat(csvRecord.get("value")),
                        Integer.parseInt(csvRecord.get("idClimaticVariable")),
                        Integer.parseInt(csvRecord.get("idCrop"))
                );

                measurements.add(measurement);
            }

            return measurements;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }
}
