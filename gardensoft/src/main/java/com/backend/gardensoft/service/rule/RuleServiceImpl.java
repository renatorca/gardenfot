package com.backend.gardensoft.service.rule;

import com.backend.gardensoft.entity.Rule;
import com.backend.gardensoft.repository.RuleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
@Service
public class RuleServiceImpl implements RuleService{
    @Autowired
    private RuleRepository ruleRepository;
    @Override
    @Transactional(readOnly = true)
    public ArrayList<Rule> findByGarden(Integer idGarden) {
        return ruleRepository.findAllByVegetableGardenIdGardenAndIsActive(idGarden, true);
    }

    @Override
    @Transactional
    public Rule createRule(Rule rule) {
        return ruleRepository.save(rule);
    }

    @Override
    @Transactional
    public Rule updateRule(Rule rule) {
        Rule original = ruleRepository.findByIdRuleAndIsActive(rule.getIdRule(), true);
        if(original!=null){
            return ruleRepository.save(rule);
        }
        return null;
    }

    @Override
    @Transactional
    public Rule deleteRule(Integer idRule) {
        Rule rule = ruleRepository.findByIdRuleAndIsActive(idRule, true);
        if(rule!=null){
            rule.setIsActive(false);
            return ruleRepository.save(rule);
        }
        return null;
    }
}
