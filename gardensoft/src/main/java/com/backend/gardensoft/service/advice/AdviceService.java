package com.backend.gardensoft.service.advice;

import com.backend.gardensoft.dto.DecisionDTO;
import com.backend.gardensoft.entity.Advice;
import com.backend.gardensoft.entity.CropControl;

public interface AdviceService {
    public Advice acceptAdvice(Integer idAdvice);
    public Advice denyAdvice(Integer idAdvice, DecisionDTO cropControl);
}
