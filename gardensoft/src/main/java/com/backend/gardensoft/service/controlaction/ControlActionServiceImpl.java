package com.backend.gardensoft.service.controlaction;

import com.backend.gardensoft.entity.ControlAction;
import com.backend.gardensoft.repository.ControlActionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
public class ControlActionServiceImpl implements ControlActionService {
    @Autowired
    private ControlActionRepository controlActionRepository;
    @Override
    @Transactional(readOnly = true)
    public ArrayList<ControlAction> findByGarden(Integer idGarden) {
        return controlActionRepository.findAllByRuleVegetableGardenIdGardenAndIsActive(idGarden, true);
    }

    @Override
    @Transactional
    public ControlAction createControlAction(ControlAction controlAction) {
        return controlActionRepository.save(controlAction);
    }

    @Override
    @Transactional
    public ControlAction updateControlAction(ControlAction controlAction) {
        ControlAction original =  controlActionRepository.findByIdControlActionAndIsActive(controlAction.getIdControlAction(), true);
        if(original!=null){
            return controlActionRepository.save(controlAction);
        }
        return null;
    }

    @Override
    @Transactional
    public ControlAction deleteControlAction(Integer idControlAction) {
        ControlAction original = controlActionRepository.findByIdControlActionAndIsActive(idControlAction, true);
        if(original!=null){
            original.setIsActive(false);
            return controlActionRepository.save(original);
        }
        return null;
    }
}
