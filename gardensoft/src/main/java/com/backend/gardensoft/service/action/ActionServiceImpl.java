package com.backend.gardensoft.service.action;

import com.backend.gardensoft.entity.Action;
import com.backend.gardensoft.repository.ActionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
public class ActionServiceImpl implements ActionService{
    @Autowired
    private ActionRepository actionRepository;
    @Override
    @Transactional(readOnly = true)
    public ArrayList<Action> getAll() {
        return actionRepository.getAllByIsActive(true);
    }
}
