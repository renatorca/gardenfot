package com.backend.gardensoft.service.action;


import com.backend.gardensoft.entity.Action;

import java.util.ArrayList;

public interface ActionService {
    public ArrayList<Action> getAll();
}
