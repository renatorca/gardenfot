package com.backend.gardensoft.service.controlaction;

import com.backend.gardensoft.entity.ControlAction;

import java.util.ArrayList;

public interface ControlActionService {
    public ArrayList<ControlAction> findByGarden (Integer idGarden);
    public ControlAction createControlAction (ControlAction controlAction);
    public ControlAction updateControlAction (ControlAction controlAction);
    public ControlAction deleteControlAction (Integer idControlAction);
}
