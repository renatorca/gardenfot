package com.backend.gardensoft.service.file;

import com.backend.gardensoft.dto.MeasurementFileDTO;
import com.backend.gardensoft.entity.Measurement;
import com.backend.gardensoft.repository.ClimaticVariableRepository;
import com.backend.gardensoft.repository.CropRepository;
import com.backend.gardensoft.repository.MeasurementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class BulkLoadServiceImpl implements BulkLoadService{
    @Autowired
    private MeasurementRepository measurementRepository;
    @Autowired
    private ClimaticVariableRepository climaticVariableRepository;
    @Autowired
    private CropRepository cropRepository;

    @Override
    public ArrayList<Measurement> save(MultipartFile file) {
        int i;
        ArrayList<Measurement> measurements = new ArrayList<>();
        try {
            List<MeasurementFileDTO> measurementsDTO = CSVHelper.csvToMeasurements(file.getInputStream());
            for(i=0; i<measurementsDTO.size(); i++){
                Measurement measurement = new Measurement();
                measurement.setMeasurementTime(measurementsDTO.get(i).getMeasurementTime());
                measurement.setOutOfThreshold(false);
                measurement.setValue(measurementsDTO.get(i).getValue());
                measurement.setClimaticVariable(climaticVariableRepository.findByIdClimaticVariable(measurementsDTO.get(i).getIdClimaticVariable()));
                measurement.setCrop(cropRepository.findByIdCropAndIsActive(measurementsDTO.get(i).getIdCrop(), true));
                measurement.setIsChecked(false);
                Measurement measurementSaved = measurementRepository.save(measurement);
                measurements.add(measurementSaved);
            }
            return measurements;
        } catch (IOException e) {
            throw new RuntimeException("fail to store csv data: " + e.getMessage());
        }
    }
}
