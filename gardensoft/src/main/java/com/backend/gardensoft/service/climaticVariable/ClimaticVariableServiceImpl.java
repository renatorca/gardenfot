package com.backend.gardensoft.service.climaticVariable;

import com.backend.gardensoft.entity.ClimaticVariable;
import com.backend.gardensoft.repository.ClimaticVariableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ClimaticVariableServiceImpl implements ClimaticVariableService{
    @Autowired
    private ClimaticVariableRepository climaticVariableRepository;
    @Override
    public ArrayList<ClimaticVariable> findAll() {
        return ((ArrayList<ClimaticVariable>) climaticVariableRepository.findAll());
    }
}
