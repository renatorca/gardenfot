package com.backend.gardensoft.service.climaticVariable;

import com.backend.gardensoft.entity.ClimaticVariable;

import java.util.ArrayList;

public interface ClimaticVariableService {
    public ArrayList<ClimaticVariable> findAll ();
}
