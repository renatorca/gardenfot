package com.backend.gardensoft.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="advice")
public class Advice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idAdvice;
    @Column
    private LocalDateTime time;
    @Column
    private Boolean isAnswered;
    @Column
    private Boolean isAccepted;
    @Column
    private LocalDateTime answerTime;
    @ManyToOne
    @JoinColumn(name = "idControlAction")
    private ControlAction controlAction;
    @ManyToOne
    @JoinColumn(name = "idUser")
    private User user;

    public Advice() {
    }

    public Advice(Integer idAdvice, LocalDateTime time, Boolean isAnswered, Boolean isAccepted, LocalDateTime answerTime, ControlAction controlAction, User user) {
        this.idAdvice = idAdvice;
        this.time = time;
        this.isAnswered = isAnswered;
        this.isAccepted = isAccepted;
        this.answerTime = answerTime;
        this.controlAction = controlAction;
        this.user = user;
    }

    public Integer getIdAdvice() {
        return idAdvice;
    }

    public void setIdAdvice(Integer idAdvice) {
        this.idAdvice = idAdvice;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public Boolean getIsAnswered() {
        return isAnswered;
    }

    public void setIsAnswered(Boolean answered) {
        isAnswered = answered;
    }

    public Boolean getIsAccepted() {
        return isAccepted;
    }

    public void setIsAccepted(Boolean accepted) {
        isAccepted = accepted;
    }

    public LocalDateTime getAnswerTime() {
        return answerTime;
    }

    public void setAnswerTime(LocalDateTime answerTime) {
        this.answerTime = answerTime;
    }

    public ControlAction getControlAction() {
        return controlAction;
    }

    public void setControlAction(ControlAction controlAction) {
        this.controlAction = controlAction;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
