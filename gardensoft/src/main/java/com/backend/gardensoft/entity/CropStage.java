package com.backend.gardensoft.entity;
import javax.persistence.*;

@Entity
@Table(name="crop_stage")
public class CropStage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idCropStage;
    @Column
    private String name;
    @Column
    private String description;

    public CropStage(){

    }
    public CropStage(Integer idCropStage, String name, String description) {
        this.idCropStage = idCropStage;
        this.name = name;
        this.description = description;
    }

    public Integer getIdCropStage() {
        return idCropStage;
    }

    public void setIdCropStage(Integer idCropStage) {
        this.idCropStage = idCropStage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
