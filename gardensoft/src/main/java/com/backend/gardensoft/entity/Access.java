package com.backend.gardensoft.entity;

import javax.persistence.*;

@Entity
@Table(name="access")
public class Access {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idAccess;
    @Column
    private Boolean isActive;
    @ManyToOne
    @JoinColumn(name = "idUser")
    private User user;
    @ManyToOne
    @JoinColumn(name = "idModule")
    private Module module;

    public Access(){

    }
    public Access(Integer idAccess, Boolean isActive, User user, Module module) {
        this.idAccess = idAccess;
        this.isActive = isActive;
        this.user = user;
        this.module = module;
    }

    public Integer getIdAccess() {
        return idAccess;
    }

    public void setIdAccess(Integer idAccess) {
        this.idAccess = idAccess;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }
}
