package com.backend.gardensoft.entity;

import javax.persistence.*;

@Entity
@Table(name="data_frame")
public class DataFrame {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idDataFrame;
    @Column
    private String onCommand;
    @Column
    private String offCommand;
    @ManyToOne
    @JoinColumn(name = "idCropControl")
    private CropControl cropControl;

    public DataFrame() {
    }

    public DataFrame(Integer idDataFrame, String onCommand, String offCommand, CropControl cropControl) {
        this.idDataFrame = idDataFrame;
        this.onCommand = onCommand;
        this.offCommand = offCommand;
        this.cropControl = cropControl;
    }

    public Integer getIdDataFrame() {
        return idDataFrame;
    }

    public void setIdDataFrame(Integer idDataFrame) {
        this.idDataFrame = idDataFrame;
    }

    public String getOnCommand() {
        return onCommand;
    }

    public void setOnCommand(String onCommand) {
        this.onCommand = onCommand;
    }

    public String getOffCommand() {
        return offCommand;
    }

    public void setOffCommand(String offCommand) {
        this.offCommand = offCommand;
    }

    public CropControl getCropControl() {
        return cropControl;
    }

    public void setCropControl(CropControl cropControl) {
        this.cropControl = cropControl;
    }
}
