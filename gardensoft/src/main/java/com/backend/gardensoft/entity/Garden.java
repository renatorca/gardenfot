package com.backend.gardensoft.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="garden")
public class Garden {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idGarden;
    @Column
    private String name;
    @Column
    private Boolean isActive;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "garden")
    private Set<Vegetable> vegetables;

    public Garden(){

    }
    public Garden(Integer idGarden, String name, Boolean isActive) {
        this.idGarden = idGarden;
        this.name = name;
        this.isActive = isActive;
    }

    public Integer getIdGarden() {
        return idGarden;
    }

    public void setIdGarden(Integer idGarden) {
        this.idGarden = idGarden;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }
}
