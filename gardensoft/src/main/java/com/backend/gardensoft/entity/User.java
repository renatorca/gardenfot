package com.backend.gardensoft.entity;

import javax.persistence.*;

@Entity
@Table(name="user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idUser;
    @Column
    private String nickname;
    @Column
    private String password;
    @Column
    private Integer role;
    @ManyToOne
    @JoinColumn(name = "idGarden")
    private Garden garden;

    public User(){

    }
    public User(Integer idUser, String nickname, String password, Integer role, Garden garden) {
        this.idUser = idUser;
        this.nickname = nickname;
        this.password = password;
        this.role = role;
        this.garden = garden;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public Garden getGarden() {
        return garden;
    }

    public void setGarden(Garden garden) {
        this.garden = garden;
    }
}
