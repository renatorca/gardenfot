package com.backend.gardensoft.entity;
import com.backend.gardensoft.dto.MeasurementDTO;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name="crop")
public class Crop {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idCrop;
    @Column
    private LocalDate startDate;
    @Column
    private LocalDate endDate;
    @Column
    private Boolean isActive;
    @ManyToOne
    @JoinColumn(name = "idVegetable")
    private Vegetable vegetable;
    @ManyToOne
    @JoinColumn(name = "idVegetableStatus")
    private VegetableStatus vegetableStatus;
    @ManyToOne
    @JoinColumn(name = "idPlot")
    private Plot plot;
    //@OneToMany(cascade = CascadeType.ALL, mappedBy = "crop")
    //private Set<MeasurementDTO> measurements;
    public Crop(){

    }

    public Crop(Integer idCrop, LocalDate startDate, LocalDate endDate, Boolean isActive, Vegetable vegetable, VegetableStatus vegetableStatus, Plot plot) {
        this.idCrop = idCrop;
        this.startDate = startDate;
        this.endDate = endDate;
        this.isActive = isActive;
        this.vegetable = vegetable;
        this.vegetableStatus = vegetableStatus;
        this.plot = plot;
    }

    public Integer getIdCrop() {
        return idCrop;
    }

    public void setIdCrop(Integer idCrop) {
        this.idCrop = idCrop;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Vegetable getVegetable() {
        return vegetable;
    }

    public void setVegetable(Vegetable vegetable) {
        this.vegetable = vegetable;
    }

    public VegetableStatus getVegetableStatus() {
        return vegetableStatus;
    }

    public void setVegetableStatus(VegetableStatus vegetableStatus) {
        this.vegetableStatus = vegetableStatus;
    }

    public Plot getPlot() {
        return plot;
    }

    public void setPlot(Plot plot) {
        this.plot = plot;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }

    /*public Set<MeasurementDTO> getMeasurements() {
        return measurements;
    }

    public void setMeasurements(Set<MeasurementDTO> measurements) {
        this.measurements = measurements;
    }*/
}
