package com.backend.gardensoft.entity;

import javax.persistence.*;

@Entity
@Table(name="action")
public class Action {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idAction;
    @Column
    private String name;
    @Column
    private Boolean isActive;

    public Action() {
    }

    public Action(Integer idAction, String name, Boolean isActive) {
        this.idAction = idAction;
        this.name = name;
        this.isActive = isActive;
    }

    public Integer getIdAction() {
        return idAction;
    }

    public void setIdAction(Integer idAction) {
        this.idAction = idAction;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }
}
