package com.backend.gardensoft.entity;

import javax.persistence.*;

@Entity
@Table(name="decision")
public class Decision {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idDecision;
    @Column
    private Boolean onAction;
    @ManyToOne
    @JoinColumn(name = "idAdvice")
    private Advice advice;
    @ManyToOne
    @JoinColumn(name = "idCropControl")
    private CropControl cropControl;

    public Decision() {
    }

    public Decision(Integer idDecision, Boolean onAction, Advice advice, CropControl cropControl) {
        this.idDecision = idDecision;
        this.onAction = onAction;
        this.advice = advice;
        this.cropControl = cropControl;
    }

    public Integer getIdDecision() {
        return idDecision;
    }

    public void setIdDecision(Integer idDecision) {
        this.idDecision = idDecision;
    }

    public Advice getAdvice() {
        return advice;
    }

    public void setAdvice(Advice advice) {
        this.advice = advice;
    }

    public CropControl getCropControl() {
        return cropControl;
    }

    public void setCropControl(CropControl cropControl) {
        this.cropControl = cropControl;
    }

    public Boolean getOnAction() {
        return onAction;
    }

    public void setOnAction(Boolean onAction) {
        this.onAction = onAction;
    }
}
