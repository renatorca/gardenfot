package com.backend.gardensoft.entity;

import javax.persistence.*;

@Entity
@Table(name="control_action")
public class ControlAction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idControlAction;
    @Column
    private String attribute;
    @Column
    private Boolean isActive;
    @Column
    private Boolean isLast;
    @Column
    private Boolean onAction;
    @ManyToOne
    @JoinColumn(name = "idRule")
    private Rule rule;
    @ManyToOne
    @JoinColumn(name = "idCropControl")
    private CropControl cropControl;

    public ControlAction() {
    }

    public ControlAction(Integer idControlAction, String attribute, Boolean isActive, Boolean isLast, Boolean onAction, Rule rule, CropControl cropControl) {
        this.idControlAction = idControlAction;
        this.attribute = attribute;
        this.isActive = isActive;
        this.isLast = isLast;
        this.onAction = onAction;
        this.rule = rule;
        this.cropControl = cropControl;
    }

    public Integer getIdControlAction() {
        return idControlAction;
    }

    public void setIdControlAction(Integer idControlAction) {
        this.idControlAction = idControlAction;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }

    public CropControl getCropControl() {
        return cropControl;
    }

    public void setCropControl(CropControl cropControl) {
        this.cropControl = cropControl;
    }

    public Boolean getIsLast() {
        return isLast;
    }

    public void setIsLast(Boolean last) {
        isLast = last;
    }

    public Boolean getOnAction() {
        return onAction;
    }

    public void setOnAction(Boolean onAction) {
        this.onAction = onAction;
    }
}
