package com.backend.gardensoft.entity;

import javax.persistence.*;

@Entity
@Table(name="fact_list")
public class FactList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idFactList;
    @ManyToOne
    @JoinColumn(name = "idMeasurement")
    private Measurement measurement;
    @ManyToOne
    @JoinColumn(name = "idAdvice")
    private Advice advice;

    public FactList() {
    }

    public FactList(Integer idFactList, Measurement measurement, Advice advice) {
        this.idFactList = idFactList;
        this.measurement = measurement;
        this.advice = advice;
    }

    public Integer getIdFactList() {
        return idFactList;
    }

    public void setIdFactList(Integer idFactList) {
        this.idFactList = idFactList;
    }

    public Measurement getMeasurement() {
        return measurement;
    }

    public void setMeasurement(Measurement measurement) {
        this.measurement = measurement;
    }

    public Advice getAdvice() {
        return advice;
    }

    public void setAdvice(Advice advice) {
        this.advice = advice;
    }
}
