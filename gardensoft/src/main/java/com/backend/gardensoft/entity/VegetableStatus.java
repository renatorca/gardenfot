package com.backend.gardensoft.entity;
import javax.persistence.*;

@Entity
@Table(name="vegetable_status")
public class VegetableStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idVegetableStatus;
    @Column
    private String name;
    @Column
    private String description;
    public VegetableStatus(){}
    public VegetableStatus(Integer idVegetableStatus, String name, String description) {
        this.idVegetableStatus = idVegetableStatus;
        this.name = name;
        this.description = description;
    }

    public Integer getIdVegetableStatus() {
        return idVegetableStatus;
    }

    public void setIdVegetableStatus(Integer idVegetableStatus) {
        this.idVegetableStatus = idVegetableStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
