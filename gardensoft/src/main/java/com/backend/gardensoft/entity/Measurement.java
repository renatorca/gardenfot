package com.backend.gardensoft.entity;
import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name="measurement")
public class Measurement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idMeasurement;
    @Column
    private Float value;
    @Column
    private LocalDateTime measurementTime;
    @Column
    private Boolean outOfThreshold;
    @Column
    private Boolean isChecked=false;
    @ManyToOne
    @JoinColumn(name = "idClimaticVariable")
    private ClimaticVariable climaticVariable;
    @ManyToOne
    @JoinColumn(name = "idCrop")
    private Crop crop;

    public Measurement(){

    }
    public Measurement(Integer idMeasurement, Float value, LocalDateTime measurementTime, Boolean outOfThreshold, ClimaticVariable climaticVariable, Crop crop, Boolean isChecked) {
        this.idMeasurement = idMeasurement;
        this.value = value;
        this.measurementTime = measurementTime;
        this.outOfThreshold = outOfThreshold;
        this.climaticVariable = climaticVariable;
        this.crop = crop;
        this.isChecked = isChecked;
    }

    public Integer getIdMeasurement() {
        return idMeasurement;
    }

    public void setIdMeasurement(Integer idMeasurement) {
        this.idMeasurement = idMeasurement;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public LocalDateTime getMeasurementTime() {
        return measurementTime;
    }

    public void setMeasurementTime(LocalDateTime measurementTime) {
        this.measurementTime = measurementTime;
    }

    public Boolean getOutOfThreshold() {
        return outOfThreshold;
    }

    public void setOutOfThreshold(Boolean outOfThreshold) {
        this.outOfThreshold = outOfThreshold;
    }

    public ClimaticVariable getClimaticVariable() {
        return climaticVariable;
    }

    public void setClimaticVariable(ClimaticVariable climaticVariable) {
        this.climaticVariable = climaticVariable;
    }

    public Crop getCrop() {
        return crop;
    }

    public void setCrop(Crop crop) {
        this.crop = crop;
    }

    public Boolean getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(Boolean checked) {
        isChecked = checked;
    }
}
