package com.backend.gardensoft.entity;

import javax.persistence.*;

@Entity
@Table(name="crop_control")
public class CropControl {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idCropControl;
    @Column
    private String name;
    @Column
    private String connectionString;
    @Column
    private String onCommand;
    @Column
    private String offCommand;
    @Column
    private Boolean isOn;
    @Column
    private Boolean isActive;
    @ManyToOne
    @JoinColumn(name = "idAction")
    private Action action;
    @ManyToOne
    @JoinColumn(name = "idCrop")
    private Crop crop;

    public CropControl() {
    }

    public CropControl(Integer idCropControl, String name, String connectionString, String onCommand, String offCommand,
                       Boolean isOn, Boolean isActive, Action action, Crop crop) {
        this.idCropControl = idCropControl;
        this.name = name;
        this.connectionString = connectionString;
        this.onCommand = onCommand;
        this.offCommand = offCommand;
        this.isOn = isOn;
        this.isActive = isActive;
        this.action = action;
        this.crop = crop;
    }

    public Integer getIdCropControl() {
        return idCropControl;
    }

    public void setIdCropControl(Integer idCropControl) {
        this.idCropControl = idCropControl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConnectionString() {
        return connectionString;
    }

    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    public Boolean getIsOn() {
        return isOn;
    }

    public void setIsOn(Boolean on) {
        isOn = on;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public Crop getCrop() {
        return crop;
    }

    public void setCrop(Crop crop) {
        this.crop = crop;
    }

    public String getOnCommand() {
        return onCommand;
    }

    public void setOnCommand(String onCommand) {
        this.onCommand = onCommand;
    }

    public String getOffCommand() {
        return offCommand;
    }

    public void setOffCommand(String offCommand) {
        this.offCommand = offCommand;
    }
}
