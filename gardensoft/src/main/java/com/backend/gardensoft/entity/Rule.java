package com.backend.gardensoft.entity;

import javax.persistence.*;

@Entity
@Table(name="rule")
public class Rule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idRule;
    @Column
    private Float minValue;
    @Column
    private Float maxValue;
    @Column
    private Boolean isActive;
    @ManyToOne
    @JoinColumn(name = "idClimaticVariable")
    private ClimaticVariable climaticVariable;
    @ManyToOne
    @JoinColumn(name = "idVegetable")
    private Vegetable vegetable;
    @ManyToOne
    @JoinColumn(name = "idCropStage")
    private CropStage cropStage;

    public Rule() {
    }

    public Rule(Integer idRule, Float minValue, Float maxValue, Boolean isActive, ClimaticVariable climaticVariable, Vegetable vegetable, CropStage cropStage) {
        this.idRule = idRule;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.isActive = isActive;
        this.climaticVariable = climaticVariable;
        this.vegetable = vegetable;
        this.cropStage = cropStage;
    }

    public Integer getIdRule() {
        return idRule;
    }

    public void setIdRule(Integer idRule) {
        this.idRule = idRule;
    }

    public Float getMinValue() {
        return minValue;
    }

    public void setMinValue(Float minValue) {
        this.minValue = minValue;
    }

    public Float getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Float maxValue) {
        this.maxValue = maxValue;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }

    public ClimaticVariable getClimaticVariable() {
        return climaticVariable;
    }

    public void setClimaticVariable(ClimaticVariable climaticVariable) {
        this.climaticVariable = climaticVariable;
    }

    public Vegetable getVegetable() {
        return vegetable;
    }

    public void setVegetable(Vegetable vegetable) {
        this.vegetable = vegetable;
    }

    public CropStage getCropStage() {
        return cropStage;
    }

    public void setCropStage(CropStage cropStage) {
        this.cropStage = cropStage;
    }
}
