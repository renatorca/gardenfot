package com.backend.gardensoft.entity;

import javax.persistence.*;

@Entity
@Table(name="predecessor_list")
public class PredecessorList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPredecessorList;
    @Column
    private Boolean isActive;
    @ManyToOne
    @JoinColumn(name = "idControlAction")
    private ControlAction controlAction;
    @ManyToOne
    @JoinColumn(name = "idPredecessorControlAction")
    private ControlAction predecessorControlAction;

    public PredecessorList() {
    }

    public PredecessorList(Integer idPredecessorList, Boolean isActive, ControlAction controlAction, ControlAction predecessorControlAction) {
        this.idPredecessorList = idPredecessorList;
        this.isActive = isActive;
        this.controlAction = controlAction;
        this.predecessorControlAction = predecessorControlAction;
    }

    public Integer getIdPredecessorList() {
        return idPredecessorList;
    }

    public void setIdPredecessorList(Integer idPredecessorList) {
        this.idPredecessorList = idPredecessorList;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }

    public ControlAction getControlAction() {
        return controlAction;
    }

    public void setControlAction(ControlAction controlAction) {
        this.controlAction = controlAction;
    }

    public ControlAction getPredecessorControlAction() {
        return predecessorControlAction;
    }

    public void setPredecessorControlAction(ControlAction predecessorControlAction) {
        this.predecessorControlAction = predecessorControlAction;
    }
}
