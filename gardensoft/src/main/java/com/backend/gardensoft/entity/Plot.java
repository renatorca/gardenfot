package com.backend.gardensoft.entity;
import javax.persistence.*;

@Entity
@Table(name="plot")
public class Plot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idPlot;
    @Column
    private String name;
    @Column
    private Boolean isUsed;
    @ManyToOne
    @JoinColumn(name = "idGarden")
    private Garden garden;
    @ManyToOne
    @JoinColumn(name = "idCropStage")
    private CropStage cropStage;

    public Plot(){

    }
    public Plot(Integer idPlot, String name, Boolean isUsed, Garden garden, CropStage cropStage) {
        this.idPlot = idPlot;
        this.name = name;
        this.isUsed = isUsed;
        this.garden = garden;
        this.cropStage = cropStage;
    }

    public Integer getIdPlot() {
        return idPlot;
    }

    public void setIdPlot(Integer idPlot) {
        this.idPlot = idPlot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(Boolean used) {
        isUsed = used;
    }

    public Garden getGarden() {
        return garden;
    }

    public void setGarden(Garden garden) {
        this.garden = garden;
    }

    public CropStage getCropStage() {
        return cropStage;
    }

    public void setCropStage(CropStage cropStage) {
        this.cropStage = cropStage;
    }
}
