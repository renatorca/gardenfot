package com.backend.gardensoft.entity;

import javax.persistence.*;

@Entity
@Table(name="vegetable")
public class Vegetable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idVegetable;
    @Column
    private String name;
    @Column
    private Boolean isActive;
    @Column
    private Integer cultivationTime;
    @Column
    private String optimalSowingMonth;
    @Column
    private Boolean isFavorite;
    @Column
    private Boolean indirectCrop;
    @Column
    private String consumableSection;
    @Column
    private String description;
    @ManyToOne
    @JoinColumn(name = "idGarden")
    private Garden garden;
    public Vegetable(){

    }
    public Vegetable(Integer idVegetable, String name, Boolean isActive, Integer cultivationTime, String optimalSowingMonth, Boolean isFavorite, Boolean indirectCrop, String consumableSection, String description, Garden garden) {
        this.idVegetable = idVegetable;
        this.name = name;
        this.isActive = isActive;
        this.cultivationTime = cultivationTime;
        this.optimalSowingMonth = optimalSowingMonth;
        this.isFavorite = isFavorite;
        this.indirectCrop = indirectCrop;
        this.consumableSection = consumableSection;
        this.description = description;
        this.garden = garden;
    }

    public Integer getIdVegetable() {
        return idVegetable;
    }

    public void setIdVegetable(Integer idVegetable) {
        this.idVegetable = idVegetable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }

    public Integer getCultivationTime() {
        return cultivationTime;
    }

    public void setCultivationTime(Integer cultivationTime) {
        this.cultivationTime = cultivationTime;
    }

    public String getOptimalSowingMonth() {
        return optimalSowingMonth;
    }

    public void setOptimalSowingMonth(String optimalSowingMonth) {
        this.optimalSowingMonth = optimalSowingMonth;
    }

    public Boolean getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(Boolean favorite) {
        isFavorite = favorite;
    }

    public Boolean getIndirectCrop() {
        return indirectCrop;
    }

    public void setIndirectCrop(Boolean indirectCrop) {
        this.indirectCrop = indirectCrop;
    }

    public String getConsumableSection() {
        return consumableSection;
    }

    public void setConsumableSection(String consumableSection) {
        this.consumableSection = consumableSection;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Garden getGarden() {
        return garden;
    }

    public void setGarden(Garden garden) {
        this.garden = garden;
    }

}
