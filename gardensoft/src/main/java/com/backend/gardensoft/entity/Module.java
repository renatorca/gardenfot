package com.backend.gardensoft.entity;

import javax.persistence.*;

@Entity
@Table(name="module")
public class Module {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idModule;
    @Column
    private String name;
    @Column
    private String description;

    public Module(){

    }
    public Module(Integer idModule, String name, String description) {
        this.idModule = idModule;
        this.name = name;
        this.description = description;
    }

    public Integer getIdModule() {
        return idModule;
    }

    public void setIdModule(Integer idModule) {
        this.idModule = idModule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
