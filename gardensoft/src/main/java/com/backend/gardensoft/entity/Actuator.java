package com.backend.gardensoft.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="actuator")
public class Actuator {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idActuator;
    @Column
    private String name;
    @Column
    private String action;
    @Column
    private LocalDateTime time;
    public Actuator(){}

    public Actuator(Integer idActuator, String name, String action, LocalDateTime time) {
        this.idActuator = idActuator;
        this.name = name;
        this.action = action;
        this.time = time;
    }

    public Integer getIdActuator() {
        return idActuator;
    }

    public void setIdActuator(Integer idActuator) {
        this.idActuator = idActuator;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }
}
