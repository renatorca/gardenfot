package com.backend.gardensoft.entity;
import javax.persistence.*;

@Entity
@Table(name="climatic_variable")
public class ClimaticVariable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idClimaticVariable;
    @Column
    private String name;
    @Column
    private String measurementUnit;

    public ClimaticVariable(){

    }
    public ClimaticVariable(Integer idClimaticVariable, String name, String measurementUnit) {
        this.idClimaticVariable = idClimaticVariable;
        this.name = name;
        this.measurementUnit = measurementUnit;
    }

    public Integer getIdClimaticVariable() {
        return idClimaticVariable;
    }

    public void setIdClimaticVariable(Integer idClimaticVariable) {
        this.idClimaticVariable = idClimaticVariable;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeasurementUnit() {
        return measurementUnit;
    }

    public void setMeasurementUnit(String measurementUnit) {
        this.measurementUnit = measurementUnit;
    }
}
