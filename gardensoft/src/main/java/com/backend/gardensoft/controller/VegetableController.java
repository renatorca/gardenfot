package com.backend.gardensoft.controller;

import com.backend.gardensoft.dto.MeasurementDTO;
import com.backend.gardensoft.entity.Vegetable;
import com.backend.gardensoft.service.vegetable.VegetableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/vegetables")
public class VegetableController {
    @Autowired
    private VegetableService vegetableService;

    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody Vegetable vegetable){
        return ResponseEntity.status(HttpStatus.CREATED).body(vegetableService.createVegetable(vegetable));
    }

    @GetMapping("/{idGarden}")
    public  ResponseEntity<?> getByGarden(@PathVariable(value="idGarden") Integer idGarden){
        ArrayList<Vegetable> list = vegetableService.findByGarden(idGarden);
        if(list!=null){
            return ResponseEntity.ok().body(list);
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/variables/{idGarden}")
    public  ResponseEntity<?> getMeasurementByFavoriteVegetable(@PathVariable(value="idGarden") Integer idGarden){
        ArrayList<MeasurementDTO> list = vegetableService.getMeasurementByFavoriteVegetable(idGarden);
        if(list!=null){
            return ResponseEntity.ok().body(list);
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/update")
    public ResponseEntity<?> update(@RequestBody Vegetable vegetable){
        return ResponseEntity.status(HttpStatus.OK).body(vegetableService.updateVegetable(vegetable));
    }

    @DeleteMapping("/{idVegetable}")
    public ResponseEntity<?> delete(@PathVariable(value="idVegetable") Integer idVegetable){
        return ResponseEntity.status(HttpStatus.OK).body(vegetableService.deleteVegetable(idVegetable));
    }

    @PostMapping("/addFavorites/{idVegetable}")
    public ResponseEntity<?> addFavorites(@PathVariable(value="idVegetable") Integer idVegetable){
        return ResponseEntity.status(HttpStatus.OK).body(vegetableService.addFavorites(idVegetable));
    }

    @PostMapping("/removeFavorites/{idVegetable}")
    public ResponseEntity<?> removeFavorites(@PathVariable(value="idVegetable") Integer idVegetable){
        return ResponseEntity.status(HttpStatus.OK).body(vegetableService.removeFavorites(idVegetable));
    }
}
