package com.backend.gardensoft.controller;

import com.backend.gardensoft.entity.ControlAction;
import com.backend.gardensoft.entity.Rule;
import com.backend.gardensoft.service.rule.RuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/rules")
public class RuleController {
    @Autowired
    private RuleService ruleService;
    @GetMapping("/{idGarden}")
    public ResponseEntity<?> getByGarden(@PathVariable(value="idGarden") Integer idGarden){
        ArrayList<Rule> list = ruleService.findByGarden(idGarden);
        if(list!=null){
            return ResponseEntity.ok().body(list);
        }
        return ResponseEntity.notFound().build();
    }
    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody Rule rule){
        return ResponseEntity.status(HttpStatus.CREATED).body(ruleService.createRule(rule));
    }
    @PostMapping("/update")
    public ResponseEntity<?> update(@RequestBody Rule rule){
        return ResponseEntity.status(HttpStatus.OK).body(ruleService.updateRule(rule));
    }
    @DeleteMapping("/{idRule}")
    public ResponseEntity<?> delete(@PathVariable(value="idRule") Integer idRule){
        return ResponseEntity.status(HttpStatus.OK).body(ruleService.deleteRule(idRule));
    }
}
