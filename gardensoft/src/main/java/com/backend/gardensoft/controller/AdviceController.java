package com.backend.gardensoft.controller;

import com.backend.gardensoft.dto.DecisionDTO;
import com.backend.gardensoft.entity.CropControl;
import com.backend.gardensoft.service.advice.AdviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/advice")
public class AdviceController {
    @Autowired
    private AdviceService adviceService;
    @PostMapping("/accept/{idAdvice}")
    public ResponseEntity<?> accept(@PathVariable(value="idAdvice") Integer idAdvice){
        return ResponseEntity.status(HttpStatus.OK).body(adviceService.acceptAdvice(idAdvice));
    }
    @PostMapping("/deny/{idAdvice}")
    public ResponseEntity<?> deny(@PathVariable(value="idAdvice") Integer idAdvice, @RequestBody DecisionDTO decision){
        return ResponseEntity.status(HttpStatus.OK).body(adviceService.denyAdvice(idAdvice, decision));
    }
}
