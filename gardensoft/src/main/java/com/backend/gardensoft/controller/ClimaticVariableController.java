package com.backend.gardensoft.controller;

import com.backend.gardensoft.entity.ClimaticVariable;
import com.backend.gardensoft.entity.ControlAction;
import com.backend.gardensoft.service.climaticVariable.ClimaticVariableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/climaticVariables")
public class ClimaticVariableController {
    @Autowired
    private ClimaticVariableService climaticVariableService;
    @GetMapping
    public ResponseEntity<?> getByGarden(){
        ArrayList<ClimaticVariable> list = climaticVariableService.findAll();
        if(list!=null){
            return ResponseEntity.ok().body(list);
        }
        return ResponseEntity.notFound().build();
    }
}
