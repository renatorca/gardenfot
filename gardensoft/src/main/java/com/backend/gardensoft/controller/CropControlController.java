package com.backend.gardensoft.controller;

import com.backend.gardensoft.entity.CropControl;
import com.backend.gardensoft.service.actuator.ActuatorService;
import com.backend.gardensoft.service.cropcontrol.CropControlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/cropControls")
public class CropControlController {
    @Autowired
    private CropControlService cropControlService;
    @Autowired
    private ActuatorService actuatorService;
    @GetMapping("/actuator")
    public ResponseEntity<?> getActuator(){
        return ResponseEntity.status(HttpStatus.OK).body(actuatorService.getAll());
    }
    @PostMapping("/actuator/update")
    public ResponseEntity<?> updateActuator(@RequestBody CropControl cropControl){
        return ResponseEntity.status(HttpStatus.OK).body(actuatorService.update(cropControl));
    }
    @GetMapping("/{idGarden}")
    public ResponseEntity<?> getByGarden(@PathVariable(value="idGarden") Integer idGarden){
        ArrayList<CropControl> list = cropControlService.getAllByGarden(idGarden, true);
        if(list!=null){
            return ResponseEntity.ok().body(list);
        }
        return ResponseEntity.notFound().build();
    }
    @GetMapping("/vegetable/{idVegetable}")
    public ResponseEntity<?> getByVegetable(@PathVariable(value="idVegetable") Integer idVegetable){
        ArrayList<CropControl> list = cropControlService.getAllByVegetable(idVegetable, true);
        if(list!=null){
            return ResponseEntity.ok().body(list);
        }
        return ResponseEntity.notFound().build();
    }
    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody CropControl cropControl){
        return ResponseEntity.status(HttpStatus.CREATED).body(cropControlService.createCropControl(cropControl));
    }
    @PostMapping("/update")
    public ResponseEntity<?> update(@RequestBody CropControl cropControl){
        return ResponseEntity.status(HttpStatus.OK).body(cropControlService.updateCropControl(cropControl));
    }
    @DeleteMapping("/delete/{idCropControl}")
    public ResponseEntity<?> delete(@PathVariable(value="idCropControl") Integer idCropControl){
        return ResponseEntity.status(HttpStatus.OK).body(cropControlService.deleteCropControl(idCropControl));
    }
}
