package com.backend.gardensoft.controller;

import com.backend.gardensoft.dto.CropAndPlot;
import com.backend.gardensoft.entity.Crop;
import com.backend.gardensoft.entity.Plot;
import com.backend.gardensoft.service.crop.CropService;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/crops")
public class CropController {
    @Autowired
    private CropService cropService;

    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody Crop crop){
        return ResponseEntity.status(HttpStatus.CREATED).body(cropService.createCrop(crop));
    }

    @GetMapping("/{idGarden}")
    public  ResponseEntity<?> getByGarden(@PathVariable(value="idGarden") Integer idGarden){
        ArrayList<Crop> list = cropService.findByGarden(idGarden);
        if(list!=null){
            return ResponseEntity.ok().body(list);
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/sown/{idGarden}")
    public  ResponseEntity<?> getByGardenAndStatus(@PathVariable(value="idGarden") Integer idGarden){
        ArrayList<Crop> list = cropService.findByGardenAndStatus(idGarden);
        if(list!=null){
            return ResponseEntity.ok().body(list);
        }
        return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/{idCrop}")
    public ResponseEntity<?> delete(@PathVariable(value="idCrop") Integer idCrop){
        return ResponseEntity.status(HttpStatus.OK).body(cropService.deleteCrop(idCrop));
    }

    @PutMapping("/harvest/{idCrop}")
    public ResponseEntity<?> harvest(@PathVariable(value="idCrop") Integer idCrop){
        return ResponseEntity.status(HttpStatus.OK).body(cropService.harvestCrop(idCrop));
    }

    @PostMapping("/changePlot")
    public ResponseEntity<?> changePlot(@RequestBody CropAndPlot body){
        return ResponseEntity.status(HttpStatus.OK).body(cropService.changePlot(body.getCrop(), body.getPlot()));
    }
}
