package com.backend.gardensoft.controller;

import com.backend.gardensoft.entity.Plot;
import com.backend.gardensoft.entity.PredecessorList;
import com.backend.gardensoft.service.predecessorlist.PredecessorListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/predecessorList")
public class PredecessorListController {
    @Autowired
    private PredecessorListService predecessorListService;
    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody PredecessorList predecessorList){
        return ResponseEntity.status(HttpStatus.CREATED).body(predecessorListService.createPredecessorList(predecessorList));
    }
    @GetMapping("/controlAction/{idControlAction}")
    public ResponseEntity<?> getByControlAction(@PathVariable(value="idControlAction") Integer idControlAction){
        ArrayList<PredecessorList> list = predecessorListService.getByControlAction(idControlAction);
        if(list!=null){
            return ResponseEntity.ok().body(list);
        }
        return ResponseEntity.notFound().build();
    }
    @GetMapping("/predecessorControlAction/{idPredecessorControlAction}")
    public ResponseEntity<?> getByPredecessorControlAction(@PathVariable(value="idPredecessorControlAction") Integer idPredecessorControlAction){
        ArrayList<PredecessorList> list = predecessorListService.getByPredecessorControlAction(idPredecessorControlAction);
        if(list!=null){
            return ResponseEntity.ok().body(list);
        }
        return ResponseEntity.notFound().build();
    }
    @DeleteMapping("/deleteAllByPredecessorControlAction/{idPredecessorControlAction}")
    public ResponseEntity<?> deleteAllByPredecessorControlAction(@PathVariable(value="idPredecessorControlAction") Integer idPredecessorControlAction){
        return ResponseEntity.status(HttpStatus.OK).body(predecessorListService.deleteAllByPredecessorControlAction(idPredecessorControlAction));
    }
    @DeleteMapping("/dependency/{idControlAction}/{idPredecessorControlAction}")
    public  ResponseEntity<?> deleteByIds(@PathVariable(value="idControlAction") Integer idControlAction, @PathVariable(value="idPredecessorControlAction") Integer idPredecessorControlAction){
        PredecessorList deleting = predecessorListService.deleteByIds(idControlAction, idPredecessorControlAction);
        if(deleting!=null){
            return ResponseEntity.ok().body(deleting);
        }
        return ResponseEntity.notFound().build();
    }

}
