package com.backend.gardensoft.controller;

import com.backend.gardensoft.entity.ControlAction;
import com.backend.gardensoft.service.controlaction.ControlActionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/controlActions")
public class ControlActionController {
    @Autowired
    private ControlActionService controlActionService;

    @GetMapping("/{idGarden}")
    public ResponseEntity<?> getByGarden(@PathVariable(value="idGarden") Integer idGarden){
        ArrayList<ControlAction> list = controlActionService.findByGarden(idGarden);
        if(list!=null){
            return ResponseEntity.ok().body(list);
        }
        return ResponseEntity.notFound().build();
    }
    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody ControlAction controlAction){
        return ResponseEntity.status(HttpStatus.CREATED).body(controlActionService.createControlAction(controlAction));
    }
    @PostMapping("/update")
    public ResponseEntity<?> update(@RequestBody ControlAction controlAction){
        return ResponseEntity.status(HttpStatus.OK).body(controlActionService.updateControlAction(controlAction));
    }
    @DeleteMapping("/{idControlAction}")
    public ResponseEntity<?> delete(@PathVariable(value="idControlAction") Integer idControlAction){
        return ResponseEntity.status(HttpStatus.OK).body(controlActionService.deleteControlAction(idControlAction));
    }
}
