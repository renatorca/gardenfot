package com.backend.gardensoft.controller;

import com.backend.gardensoft.entity.Plot;
import com.backend.gardensoft.entity.Vegetable;
import com.backend.gardensoft.service.plot.PlotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/plots")
public class PlotController {
    @Autowired
    private PlotService plotService;

    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestBody Plot plot){
        return ResponseEntity.status(HttpStatus.CREATED).body(plotService.createPlot(plot));
    }

    @GetMapping("/{idGarden}/{idCropStage}")
    public  ResponseEntity<?> findByGardenAndStage(@PathVariable(value="idGarden") Integer idGarden, @PathVariable(value="idCropStage") Integer idCropStage){
        ArrayList<Plot> list = plotService.findByGardenAndStage(idGarden, idCropStage);
        if(list!=null){
            return ResponseEntity.ok().body(list);
        }
        return ResponseEntity.notFound().build();
    }
    @GetMapping("/findByGarden/{idPlot}/{idGarden}")
    public  ResponseEntity<?> findByGarden(@PathVariable(value="idPlot") Integer idPlot, @PathVariable(value="idGarden") Integer idGarden){
        ArrayList<Plot> list = plotService.findByGarden(idPlot, idGarden);
        if(list!=null){
            return ResponseEntity.ok().body(list);
        }
        return ResponseEntity.notFound().build();
    }
}
