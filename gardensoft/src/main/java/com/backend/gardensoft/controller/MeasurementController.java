package com.backend.gardensoft.controller;

import com.backend.gardensoft.dto.AdviceDTO;
import com.backend.gardensoft.entity.CropControl;
import com.backend.gardensoft.entity.Measurement;
import com.backend.gardensoft.entity.Rule;
import com.backend.gardensoft.service.file.BulkLoadService;
import com.backend.gardensoft.service.file.CSVHelper;
import com.backend.gardensoft.service.measurement.MeasurementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/measurement")
public class MeasurementController {
    @Autowired
    private MeasurementService measurementService;
    @Autowired
    private BulkLoadService bulkLoadService;
    @GetMapping("/advice/{idGarden}")
    public ResponseEntity<?> advice(@PathVariable(value="idGarden") Integer idGarden){
        AdviceDTO action = measurementService.advice(idGarden);
        if(action!=null){
            return ResponseEntity.ok().body(action);
        }
        return ResponseEntity.notFound().build();
    }
    @PostMapping("/bulkLoad")
    public ResponseEntity<?> bulkLoad(@RequestParam("file") MultipartFile file){
        if(!CSVHelper.hasCSVFormat(file)){
            ArrayList<Measurement> measurements = bulkLoadService.save(file);
            if(measurements!=null){
                return ResponseEntity.ok().body(measurements);
            }
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.notFound().build();
    }
}
