package com.backend.gardensoft.controller;

import com.backend.gardensoft.entity.Action;
import com.backend.gardensoft.service.action.ActionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/actions")
public class ActionController {
    @Autowired
    private ActionService actionService;
    @GetMapping
    public ResponseEntity<?> getByGarden(){
        ArrayList<Action> list = actionService.getAll();
        if(list!=null){
            return ResponseEntity.ok().body(list);
        }
        return ResponseEntity.notFound().build();
    }
}
