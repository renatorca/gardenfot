package com.backend.gardensoft.repository;

import com.backend.gardensoft.entity.Rule;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface RuleRepository extends JpaRepository<Rule, Integer> {
    ArrayList<Rule> findAllByVegetableGardenIdGardenAndIsActive(Integer idGarden, Boolean isActive);
    Rule findByIdRuleAndIsActive(Integer idRule, Boolean isActive);
}
