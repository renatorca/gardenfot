package com.backend.gardensoft.repository;

import com.backend.gardensoft.entity.Measurement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface MeasurementRepository extends JpaRepository<Measurement, Integer> {
    ArrayList<Measurement> getAllByCropIdCrop(Integer idCrop);
    ArrayList<Measurement> getAllByIdMeasurementNotAndCropIdCropAndIsCheckedOrderByCropIdCropAsc(Integer idMeasurement, Integer idCrop, Boolean isChecked);
    ArrayList<Measurement> getAllByCropVegetableGardenIdGardenAndIsCheckedOrderByCropIdCropAsc(Integer idGarden, Boolean isChecked);
}
