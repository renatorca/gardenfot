package com.backend.gardensoft.repository;

import com.backend.gardensoft.entity.Vegetable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface VegetableRepository extends JpaRepository<Vegetable, Integer> {
    ArrayList<Vegetable> findAllByGardenIdGardenAndIsActive(Integer idGarden, Boolean isActive);
    Vegetable findByIdVegetableAndIsActive(Integer idVegetable, Boolean isActive);
}
