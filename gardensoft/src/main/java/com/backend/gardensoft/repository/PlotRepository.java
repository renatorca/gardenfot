package com.backend.gardensoft.repository;

import com.backend.gardensoft.entity.Plot;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface PlotRepository extends JpaRepository<Plot, Integer> {
    ArrayList<Plot> findAllByIsUsedAndGardenIdGardenAndCropStageIdCropStage(Boolean idUsed, Integer idGarden, Integer idCropStage);
    ArrayList<Plot> findAllByIsUsedAndIdPlotNotAndGardenIdGarden(Boolean isUsed, Integer idPlot, Integer idGarden);
}
