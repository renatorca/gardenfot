package com.backend.gardensoft.repository;

import com.backend.gardensoft.entity.ControlAction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface ControlActionRepository extends JpaRepository<ControlAction, Integer> {
    ArrayList<ControlAction> findAllByRuleVegetableGardenIdGardenAndIsActive(Integer idGarden, Boolean isActive);
    ArrayList<ControlAction> findAllByRuleVegetableGardenIdGardenAndIsLastAndIsActiveOrderByCropControlCropIdCropAsc(Integer idGarden, Boolean isLast, Boolean isActive);
    ControlAction findByIdControlActionAndIsActive(Integer idControlAction, Boolean isActive);
}
