package com.backend.gardensoft.repository;

import com.backend.gardensoft.entity.FactList;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FactListRepository extends JpaRepository<FactList, Integer> {
}
