package com.backend.gardensoft.repository;

import com.backend.gardensoft.entity.Action;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface ActionRepository extends JpaRepository<Action, Integer> {
    ArrayList<Action> getAllByIsActive(Boolean isActive);
}
