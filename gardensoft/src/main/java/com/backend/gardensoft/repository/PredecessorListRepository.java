package com.backend.gardensoft.repository;

import com.backend.gardensoft.entity.PredecessorList;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface PredecessorListRepository extends JpaRepository<PredecessorList, Integer> {
    PredecessorList findByIdPredecessorListAndIsActive(Integer idPredecessorList, Boolean isActive);
    ArrayList<PredecessorList> findAllByControlActionIdControlActionAndIsActive(Integer idControlAction, Boolean isActive);
    ArrayList<PredecessorList> findAllByPredecessorControlActionIdControlActionAndIsActive(Integer idPredecessorControlAction, Boolean isActive);
    PredecessorList findByControlActionIdControlActionAndPredecessorControlActionIdControlActionAndIsActive(Integer idControlAction, Integer idPredecessorControlAction, Boolean isActive);
}
