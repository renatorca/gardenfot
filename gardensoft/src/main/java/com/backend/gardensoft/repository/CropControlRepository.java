package com.backend.gardensoft.repository;

import com.backend.gardensoft.entity.CropControl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface CropControlRepository extends JpaRepository<CropControl, Integer> {
    ArrayList<CropControl> findAllByCropVegetableGardenIdGardenAndIsActive(Integer idGarden, Boolean isActive);
    ArrayList<CropControl> findAllByCropVegetableIdVegetableAndIsActive(Integer idVegetable, Boolean isActive);
    ArrayList<CropControl> findAllByCropIdCropAndIsActive(Integer idCrop, Boolean isActive);
    CropControl findByIdCropControlAndIsActive(Integer idCropControl, Boolean isActive);
}
