package com.backend.gardensoft.repository;

import com.backend.gardensoft.entity.Crop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface CropRepository extends JpaRepository<Crop, Integer> {
    ArrayList<Crop> findAllByVegetableGardenIdGardenAndIsActive(Integer idGarden, Boolean isActive);
    ArrayList<Crop> findAllByVegetableGardenIdGardenAndVegetableStatusIdVegetableStatusAndIsActive(Integer idGarden, Integer idVegetableStatus, Boolean isActive);
    Crop findByIdCropAndIsActive(Integer idCrop, Boolean isActive);
}
