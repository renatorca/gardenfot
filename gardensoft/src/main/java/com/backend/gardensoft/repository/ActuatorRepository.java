package com.backend.gardensoft.repository;

import com.backend.gardensoft.entity.Actuator;
import com.backend.gardensoft.entity.ControlAction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActuatorRepository extends JpaRepository<Actuator, Integer> {
    Actuator findByIdActuator(Integer idActuator);
}
