package com.backend.gardensoft.repository;

import com.backend.gardensoft.entity.Advice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdviceRepository extends JpaRepository<Advice, Integer> {
    Advice findByIdAdvice(Integer idAdvice);
}
