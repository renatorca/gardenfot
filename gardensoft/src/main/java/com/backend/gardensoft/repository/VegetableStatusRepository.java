package com.backend.gardensoft.repository;

import com.backend.gardensoft.entity.VegetableStatus;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VegetableStatusRepository extends JpaRepository<VegetableStatus, Integer> {
    VegetableStatus findByName(String name);
}
