package com.backend.gardensoft.repository;

import com.backend.gardensoft.entity.Decision;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DecisionRepository extends JpaRepository<Decision, Integer> {
}
