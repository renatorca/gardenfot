package com.backend.gardensoft.repository;

import com.backend.gardensoft.entity.ClimaticVariable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface ClimaticVariableRepository extends JpaRepository<ClimaticVariable, Integer> {
    ClimaticVariable findByIdClimaticVariable(Integer idClimaticVariable);
}
