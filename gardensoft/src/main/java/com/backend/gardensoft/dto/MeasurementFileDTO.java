package com.backend.gardensoft.dto;

import java.time.LocalDateTime;

public class MeasurementFileDTO {
    private Integer idMeasurement;
    private LocalDateTime measurementTime;
    private Float value;
    private Integer idClimaticVariable;
    private Integer idCrop;

    public MeasurementFileDTO() {
    }

    public MeasurementFileDTO(Integer idMeasurement, LocalDateTime measurementTime, Float value, Integer idClimaticVariable, Integer idCrop) {
        this.idMeasurement = idMeasurement;
        this.measurementTime = measurementTime;
        this.value = value;
        this.idClimaticVariable = idClimaticVariable;
        this.idCrop = idCrop;
    }

    public Integer getIdMeasurement() {
        return idMeasurement;
    }

    public void setIdMeasurement(Integer idMeasurement) {
        this.idMeasurement = idMeasurement;
    }

    public LocalDateTime getMeasurementTime() {
        return measurementTime;
    }

    public void setMeasurementTime(LocalDateTime measurementTime) {
        this.measurementTime = measurementTime;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public Integer getIdClimaticVariable() {
        return idClimaticVariable;
    }

    public void setIdClimaticVariable(Integer idClimaticVariable) {
        this.idClimaticVariable = idClimaticVariable;
    }

    public Integer getIdCrop() {
        return idCrop;
    }

    public void setIdCrop(Integer idCrop) {
        this.idCrop = idCrop;
    }
}
