package com.backend.gardensoft.dto;

import com.backend.gardensoft.entity.Crop;
import com.backend.gardensoft.entity.Plot;

public class CropAndPlot {
    private Crop crop;
    private Plot plot;

    public CropAndPlot() {

    }

    public CropAndPlot(Crop crop, Plot plot) {
        this.crop = crop;
        this.plot = plot;
    }

    public Crop getCrop() {
        return crop;
    }

    public void setCrop(Crop crop) {
        this.crop = crop;
    }

    public Plot getPlot() {
        return plot;
    }

    public void setPlot(Plot plot) {
        this.plot = plot;
    }
}
