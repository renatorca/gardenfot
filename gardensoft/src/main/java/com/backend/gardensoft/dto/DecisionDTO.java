package com.backend.gardensoft.dto;

import com.backend.gardensoft.entity.CropControl;

public class DecisionDTO {
    private CropControl cropControl;
    private Boolean onAction;

    public DecisionDTO() {
    }

    public DecisionDTO(CropControl cropControl, Boolean onAction) {
        this.cropControl = cropControl;
        this.onAction = onAction;
    }

    public CropControl getCropControl() {
        return cropControl;
    }

    public void setCropControl(CropControl cropControl) {
        this.cropControl = cropControl;
    }

    public Boolean getOnAction() {
        return onAction;
    }

    public void setOnAction(Boolean onAction) {
        this.onAction = onAction;
    }
}
