package com.backend.gardensoft.dto;

import com.backend.gardensoft.entity.ClimaticVariable;
import com.backend.gardensoft.entity.Measurement;
import com.backend.gardensoft.entity.Vegetable;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class MeasurementDTO {
    private Vegetable vegetable;
    private ArrayList<Measurement> measurements;

    public MeasurementDTO() {
    }

    public Vegetable getVegetable() {
        return vegetable;
    }

    public void setVegetable(Vegetable vegetable) {
        this.vegetable = vegetable;
    }

    public ArrayList<Measurement> getMeasurements() {
        return measurements;
    }

    public void setMeasurements(ArrayList<Measurement> measurements) {
        this.measurements = measurements;
    }
}
