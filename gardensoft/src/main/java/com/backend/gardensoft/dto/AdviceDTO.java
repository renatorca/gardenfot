package com.backend.gardensoft.dto;

import com.backend.gardensoft.entity.CropControl;
import com.backend.gardensoft.entity.Measurement;

import java.util.ArrayList;

public class AdviceDTO {
    private CropControl cropControl;
    private ArrayList<Measurement> measurements;
    private Boolean onAction;
    private Integer idAdvice;

    public AdviceDTO() {
    }

    public AdviceDTO(CropControl cropControl, ArrayList<Measurement> measurements, Boolean onAction, Integer idAdvice) {
        this.cropControl = cropControl;
        this.measurements = measurements;
        this.onAction = onAction;
        this.idAdvice = idAdvice;
    }

    public CropControl getCropControl() {
        return cropControl;
    }

    public void setCropControl(CropControl cropControl) {
        this.cropControl = cropControl;
    }

    public ArrayList<Measurement> getMeasurements() {
        return measurements;
    }

    public void setMeasurements(ArrayList<Measurement> measurements) {
        this.measurements = measurements;
    }

    public Boolean getOnAction() {
        return onAction;
    }

    public void setOnAction(Boolean onAction) {
        this.onAction = onAction;
    }

    public Integer getIdAdvice() {
        return idAdvice;
    }

    public void setIdAdvice(Integer idAdvice) {
        this.idAdvice = idAdvice;
    }
}
